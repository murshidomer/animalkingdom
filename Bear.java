import java.awt.*;

public class Bear extends Critter {
    static int movesDone;
    static int stringCount =0;
    boolean polar;

    public Bear(boolean polar) {
        this.polar = polar;
    }

    public Color getColor(boolean polar) {
        if (polar)
            return Color.WHITE;
        else return Color.BLACK;
    }

    @Override
    public String toString() {
        String bearString;
        if ((stringCount % 2 == 0)) {
            bearString = "\\";
        } else {
            bearString = "/";
        }
        stringCount++;
        return bearString;
    }

    @Override
    public Action getMove(CritterInfo info) {
        if (info.frontThreat()) {
            return Action.INFECT;
        } else if (!(info.backThreat() || info.rightThreat() || info.leftThreat())) {
            return Action.HOP;
        } else {
            movesDone++;
            return Action.LEFT;
        }
    }
}
