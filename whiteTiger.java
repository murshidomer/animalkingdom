import java.awt.*;

public class whiteTiger extends Critter {
    static int infectCount;

    public whiteTiger() {
    }

    @Override
    public Color getColor() {
        final Color whiteTigerColor = Color.WHITE;
        return whiteTigerColor;
    }

    @Override
    public String toString() {
        //if infected another critter
        if (!(infectCount == 0))
            return "tgr";
        else
            return "TGR";
    }

    @Override
    public Action getMove(CritterInfo info) {
        if (info.frontThreat()) {
            infectCount++;
            return Action.INFECT;
        } else if (info.getFront().equals(Neighbor.WALL) || info.getRight().equals(Neighbor.WALL)) {
            return Action.LEFT;
        } else if (info.getFront().equals(Neighbor.SAME))
            return Action.RIGHT;
        else
            return Action.HOP;
    }
}
