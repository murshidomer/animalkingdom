import java.awt.*;

public class ninjaCat extends Critter{
    public ninjaCat() {
    }

    @Override
    public Color getColor() {
        final Color catColor = Color.white;
        return catColor;
    }

    @Override
    public Action getMove(CritterInfo info) {
        return Action.HOP;
    }

    //this line
    //another line

    @Override
    public String toString() {
        final String ninjaString = "ninja";
        return ninjaString;

    }
}
