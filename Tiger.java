import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class Tiger extends Critter {

    public Tiger() {
    }

    @Override
    public Color getColor() {
        //must be equally likely
        Color color;
        int colornumber = ThreadLocalRandom.current().nextInt(1, 4);
        switch (colornumber) {
            case 1:
                color = Color.RED;
                break;
            case 2:
                color = Color.GREEN;
                break;
            case 3:
                color = Color.BLUE;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + colornumber);
        }
        return color;
    }

    @Override
    public String toString() {
        final String tigerString = "TGR";
        return tigerString;
    }

    @Override
    public Action getMove(CritterInfo info) {
        if (info.frontThreat()) {
            return Action.INFECT;
        } else if (info.getFront().equals(Neighbor.WALL) || info.getRight().equals(Neighbor.WALL)) {
            return Action.LEFT;
        } else if (info.getFront().equals(Neighbor.SAME))
            return Action.RIGHT;
        else
            return Action.HOP;
    }
}
