import java.awt.*;

public class Giant extends Critter {
    static int moves;
    static int moveSet = 0;

    public Giant() {
    }

    @Override
    public Color getColor() {
        final Color giantColor = Color.gray;
        return giantColor;
    }

    @Override
    public Action getMove(CritterInfo info) {
        moves++;
        if (info.frontThreat())
            return Action.INFECT;
        else if (!(info.backThreat() || info.rightThreat() || info.leftThreat()))
            return Action.HOP;
        else return Action.RIGHT;
    }

    @Override
    public String toString() {
        String moveString = "";
        int count = 0;
        if (moveSet < 1) moveSet = 1;
        if (moves % 24 == 0) moveSet = moves / 24;
        if (moves <= (24 * moveSet) / 4)
            moveString = "fee";
        else if ((moves > (24 * moveSet) / 4) && (moves <= (24 * moveSet) * 2 / 4))
            moveString = "fie";
        else if ((moves > (24 * moveSet) * 2 / 4) && (moves <= (24 * moveSet) * 3 / 4))
            moveString = "foe";
        else if ((moves > (24 * moveSet) * 3 / 4) && (moves <= (24 * moveSet) * 4 / 4))
            moveString = "fum";
        return moveString;

    /*    for (int i = 0; i < 4; i++) {
            for (int j = 0; j <= 6; j++) {
                if (i == 0) {
                    moveString = "fee";
                } else if (i == 1) moveString = "fie";
                else if (i == 2) moveString = "foe";
                else if (i == 3) moveString = "fum";
            }
            count++;
        }
        return moveString;
    }*/

        //this moves down
        //this moves up
    }
}


